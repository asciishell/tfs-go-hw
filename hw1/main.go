package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
	"time"
)

const dataset string = "data/trades.csv"
const timeFormat = "2006-01-02 15:04:05"

type stock struct {
	ticker string
	price  float64
	number uint32
	date   time.Time
}

type candle struct {
	ticker     string
	date       time.Time
	priceStart float64
	priceEnd   float64
	priceMin   float64
	priceMax   float64
}

func checkErr(err error, mess string) {
	if err != nil {
		fmt.Println(mess)
		fmt.Println(err)
		os.Exit(1)
	}
}

func validateHour(t time.Time) bool {
	return t.Hour() >= 7
}
func loadData(filename string) []stock {
	file, err := os.Open(filename)
	checkErr(err, "Could not open file")
	defer func() {
		err := file.Close()
		checkErr(err, "File closing error")
	}()

	lines, err := csv.NewReader(file).ReadAll()
	checkErr(err, "Could not parse csv")
	data := make([]stock, 0, len(lines))
	for _, line := range lines {

		t, errTime := time.Parse(timeFormat, line[3])
		checkErr(errTime, "Could not parse time "+line[3])
		price, errPrice := strconv.ParseFloat(line[1], 64)
		checkErr(errPrice, "Could not parse float price "+line[1])
		number, errNumber := strconv.ParseUint(line[2], 10, 64)
		checkErr(errNumber, "Could not parse number value "+line[2])

		if validateHour(t) {
			data = append(data, stock{line[0], float64(price), uint32(number), t})
		}
	}
	return data
}

func flush(target []candle, candles map[string]*candle) []candle {
	for _, c := range candles {
		target = append(target, *c)
	}
	return target
}

func groupData(data []stock, period time.Duration) []candle {
	result := make([]candle, 0, len(data))
	start := time.Date(data[0].date.Year(), data[0].date.Month(), data[0].date.Day(), 7, 0, 0, 0, time.UTC)
	end := start.Add(period)
	currentCandles := make(map[string]*candle)
	for _, v := range data {
		if v.date.After(end) {
			for {
				start = end
				end = end.Add(period)
				if v.date.Before(end) && validateHour(start) {
					break
				}
			}
			result = flush(result, currentCandles)
			currentCandles = make(map[string]*candle)
		}
		if v.date.Before(start) {
			continue
		}
		if _, ok := currentCandles[v.ticker]; !ok {
			currentCandles[v.ticker] = &candle{v.ticker,
				start, v.price, v.price, v.price, v.price}
		}
		currentCandles[v.ticker].priceEnd = v.price
		switch {
		case currentCandles[v.ticker].priceMin > v.price:
			currentCandles[v.ticker].priceMin = v.price
		case currentCandles[v.ticker].priceMax < v.price:
			currentCandles[v.ticker].priceMax = v.price
		}

	}
	result = flush(result, currentCandles)
	return result
}

func saveData(data []candle, filename string) {
	file, err := os.Create(filename)
	checkErr(err, "Could not write to file")
	defer func() {
		err := file.Close()
		checkErr(err, "File closing error")
	}()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, value := range data {
		row := []string{value.ticker,
			value.date.Format(time.RFC3339),
			strconv.FormatFloat(float64(value.priceStart), 'f', -1, 64),
			strconv.FormatFloat(float64(value.priceMax), 'f', -1, 64),
			strconv.FormatFloat(float64(value.priceMin), 'f', -1, 64),
			strconv.FormatFloat(float64(value.priceEnd), 'f', -1, 64)}
		err := writer.Write(row)
		checkErr(err, "Could not write row")
	}
}
func main() {

	data := loadData(dataset)
	saveData(groupData(data, time.Minute*time.Duration(5)), "candles_5min.csv")
	saveData(groupData(data, time.Minute*time.Duration(30)), "candles_30min.csv")
	saveData(groupData(data, time.Minute*time.Duration(240)), "candles_240min.csv")
}
