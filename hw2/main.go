package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"sort"
	"time"
)

type NewsGetter interface {
	GetPublished() time.Time
	GetTickers() []string
}
type News struct {
	ID          int64     `json:"id"`
	Title       string    `json:"title"`
	Body        string    `json:"body"`
	PublishedAt time.Time `json:"published_at"`
	Provider    string    `json:"provider"`
	Tickers     []string  `json:"tickers"`
}

func (n News) GetPublished() time.Time {
	return n.PublishedAt
}
func (n News) GetTickers() []string {
	return n.Tickers
}

type NewsCollection struct {
	News        []News    `json:"items"`
	PublishedAt time.Time `json:"published_at"`
	Tickers     []string  `json:"tickers"`
}

func (n NewsCollection) GetPublished() time.Time {
	return n.PublishedAt
}
func (n NewsCollection) GetTickers() []string {
	return n.Tickers
}

type NewsGroup struct {
	Type    string     `json:"type"`
	Payload NewsGetter `json:"payload"`
}

const GroupTypeCompany = "company_news"

func EqualTickers(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	setA := make(map[string]bool)
	for _, vA := range a {
		setA[vA] = true
	}
	for _, vB := range b {
		if !setA[vB] {
			return false
		}
	}
	return true
}

func EqualDate(a, b time.Time) bool {
	return a.Year() == b.Year() && a.Month() == b.Month() && a.Day() == b.Day()
}

func LoadNews(filename string) ([]News, error) {
	plan, err := ioutil.ReadFile(filepath.Clean(filename))
	if err != nil {
		return nil, fmt.Errorf("can't open file %s: %s", filename, err)
	}
	var data []News
	err = json.Unmarshal(plan, &data)
	if err != nil {
		return nil, fmt.Errorf("can't parse file %s: %s", filename, err)
	}
	return data, nil
}

func SortNews(data []News) []News {
	sort.SliceStable(data, func(i, j int) bool {
		return data[i].PublishedAt.Before(data[j].PublishedAt)
	})
	return data
}
func GroupNews(data []News) []NewsGroup {
	var result []NewsGroup
	currDateIndex := 0
	for i, value := range data {
		isAdd := false
		for j := currDateIndex; j < len(result); j++ {
			if !EqualDate(result[currDateIndex].Payload.GetPublished(), value.PublishedAt) {
				currDateIndex = len(result)
				break
			}
			if EqualDate(value.PublishedAt, result[j].Payload.GetPublished()) && EqualTickers(value.Tickers, result[j].Payload.GetTickers()) {
				switch currNews := result[j].Payload.(type) {
				case News:
					newsList := make([]News, 0)
					newsList = append(newsList, currNews)
					newsList = append(newsList, data[i])
					result[j].Type = GroupTypeCompany
					result[j].Payload = NewsCollection{News: newsList, PublishedAt: currNews.PublishedAt, Tickers: currNews.Tickers}
				case NewsCollection:
					currNews.News = append(currNews.News, data[i])
					if currNews.PublishedAt.After(value.PublishedAt) {
						currNews.PublishedAt = value.PublishedAt
					}
					result[j].Payload = currNews
				}
				isAdd = true
				break
			}
		}
		if !isAdd {
			// Add to list
			result = append(result, NewsGroup{Type: "news", Payload: data[i]})
		}
	}
	return result
}
func SaveNews(data []NewsGroup, filename string) error {
	rankingsJSON, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		return fmt.Errorf("can't marshal data: %s", err)
	}
	err = ioutil.WriteFile(filename, rankingsJSON, 0644)
	if err != nil {
		return fmt.Errorf("can't save to file %s: %s", filename, err)
	}
	return nil
}
func main() {
	var filename string
	flag.StringVar(&filename, "file", "news.json", "Source file name")
	flag.Parse()
	data, err := LoadNews(filename)
	if err != nil {
		log.Fatalf("can't load file: %s", err)
	}
	groupedNews := GroupNews(SortNews(data))
	sort.SliceStable(groupedNews, func(i, j int) bool {
		if groupedNews[i].Payload.GetPublished().Equal(groupedNews[j].Payload.GetPublished()) {
			return groupedNews[i].Type == GroupTypeCompany
		}
		return groupedNews[i].Payload.GetPublished().Before(groupedNews[j].Payload.GetPublished())
	})
	err = SaveNews(groupedNews, "out.json")
	if err != nil {
		log.Fatalf("can't save file: %s", err)
	}
}
