package main

import (
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"time"
)

const timeFormat = "2006-01-02 15:04:05"

type stock struct {
	ticker string
	price  float64
	number uint32
	date   time.Time
}

type candle struct {
	ticker     string
	date       time.Time
	priceStart float64
	priceEnd   float64
	priceMin   float64
	priceMax   float64
}

func validateHour(t time.Time) bool {
	return t.Hour() >= 7
}
func stockToChannels(data []chan stock, value stock) {
	if validateHour(value.date) {
		for i := 0; i < len(data); i++ {
			data[i] <- value
		}
	}
}
func loadData(file io.Reader, count int) ([]chan stock, chan error) {
	reader := csv.NewReader(file)
	out := make([]chan stock, count)
	for i := 0; i < count; i++ {
		out[i] = make(chan stock)
	}
	outErr := make(chan error)
	go func() {
		defer func() {
			for i := 0; i < count; i++ {
				close(out[i])
			}
			close(outErr)
		}()
		for {
			line, err := reader.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				outErr <- fmt.Errorf("error reading from csv document: %s", err)
				return
			}
			t, err := time.Parse(timeFormat, line[3])
			if err != nil {
				outErr <- fmt.Errorf("can't parse time %s: %s", line[3], err)
				return
			}
			price, err := strconv.ParseFloat(line[1], 64)
			if err != nil {
				outErr <- fmt.Errorf("can't parse float %s: %s", line[1], err)
				return
			}
			number, err := strconv.ParseUint(line[2], 10, 64)
			if err != nil {
				outErr <- fmt.Errorf("can't parse uint %s: %s", line[2], err)
				return
			}
			stockToChannels(out, stock{line[0], price, uint32(number), t})
		}
	}()
	return out, outErr
}

func mapToChan(candles map[string]*candle, out chan candle) {
	for _, c := range candles {
		out <- *c
	}
}
func EqualDay(a, b time.Time) bool {
	return a.Year() == b.Year() && a.Month() == b.Month() && a.Day() == b.Day()
}
func HandleCandle(currentCandles map[string]*candle, start time.Time, value stock) {
	if _, ok := currentCandles[value.ticker]; !ok {
		currentCandles[value.ticker] = &candle{value.ticker, start, value.price, value.price, value.price, value.price}
	}
	currentCandles[value.ticker].priceEnd = value.price
	switch {
	case currentCandles[value.ticker].priceMin > value.price:
		currentCandles[value.ticker].priceMin = value.price
	case currentCandles[value.ticker].priceMax < value.price:
		currentCandles[value.ticker].priceMax = value.price
	}
}
func groupData(data <-chan stock, period time.Duration) <-chan candle {
	out := make(chan candle)
	go func() {
		defer close(out)
		var start, end time.Time
		currentCandles := make(map[string]*candle)
		for v := range data {
			if !EqualDay(v.date, start) {
				start = time.Date(v.date.Year(), v.date.Month(), v.date.Day(), 7, 0, 0, 0, time.UTC)
				end = start.Add(period)
				mapToChan(currentCandles, out)
				currentCandles = make(map[string]*candle)
			}
			if v.date.After(end) {
				for {
					start = end
					end = end.Add(period)
					if v.date.Before(end) && validateHour(start) {
						break
					}
				}
				mapToChan(currentCandles, out)
				currentCandles = make(map[string]*candle)
			}
			if v.date.Before(start) {
				continue
			}
			HandleCandle(currentCandles, start, v)
		}
		mapToChan(currentCandles, out)
	}()
	return out
}

func saveData(data <-chan candle, file io.Writer, done chan interface{}) chan error {
	outErr := make(chan error)
	go func() {
		defer close(outErr)
		writer := csv.NewWriter(file)
		defer writer.Flush()

		for value := range data {
			row := []string{value.ticker,
				value.date.Format(time.RFC3339),
				strconv.FormatFloat(value.priceStart, 'f', -1, 64),
				strconv.FormatFloat(value.priceMax, 'f', -1, 64),
				strconv.FormatFloat(value.priceMin, 'f', -1, 64),
				strconv.FormatFloat(value.priceEnd, 'f', -1, 64)}
			err := writer.Write(row)
			if err != nil {
				outErr <- fmt.Errorf("can't write row %v: %s", row, err)
				return
			}

		}
		done <- struct{}{}
	}()
	return outErr
}

func WaitDone(timeout context.Context, done <-chan interface{}, count int) bool {
	for {
		select {
		case <-done:
			count--
			if count == 0 {
				return true
			}
		case <-timeout.Done():
			return false
		}
	}

}
func main() {
	var periods = []int{5, 30, 240}
	var filename string
	flag.StringVar(&filename, "file", "trades.csv", "Source file name")
	flag.Parse()

	fileIn, err := os.Open(filename)
	if err != nil {
		log.Fatalf("can't open file %s: %s", filename, err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	dataChan, errChan := loadData(fileIn, len(periods))
	done := make(chan interface{})
	required := len(periods)
	fileOut := make([]*os.File, len(periods))
	for i, v := range periods {
		fileOut[i], err = os.Create(fmt.Sprintf("candles_%dmin.csv", v))
		if err != nil {
			log.Fatalf("can't create file %s: %s", filename, err)
		}
		saveData(groupData(dataChan[i], time.Minute*time.Duration(v)), fileOut[i], done)
	}
	defer func() {
		_ = fileIn.Close()
		for i := range periods {
			_ = fileOut[i].Close()
		}
		for err := range errChan {
			log.Println(err)
		}
	}()
	if WaitDone(ctx, done, required) {
		log.Println("Done")
	} else {
		log.Fatal("Timeout")
	}
}
